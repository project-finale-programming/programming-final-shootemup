/*
Enemy
 
 Red planes represent enemy planes
 
 */

class Enemy {
  //variables to hold enemy position, image, and size
  PVector position;
  PImage enemyPlaneImage;
  PVector enemySize = new PVector (35, 35);
  float speed;
  
 //there will be a cooldown for shooting, and number of bullets on the window will also be limited
 float bulletMax = 2, shootingCooldown, shootingCooldownMax = 0.5;

 //we will store all our enemyBullets in this ArrayList
 ArrayList<EnemyBullet> bullets;


  //constructor calls the initialize function 
  Enemy() {
    enemyPlaneImage = loadImage("Enemy.png");
    initialize();
    
    //initialize bullets ArrayList
    bullets = new ArrayList<EnemyBullet>();
    
    shootingCooldown = shootingCooldownMax;
  }
  
 //move enemy position to left side of screen and draw
 void display()
 {
    if (position.x <= 0) {
    //if yes, respawn the enemy
    initialize();
    }
   //move and display enemy
   position.x -=speed;   
   imageMode(CENTER);
   image(enemyPlaneImage, position.x, position.y, enemySize.x, enemySize.y);
   
    //reduce shootingCooldown
    shootingCooldown -= (1/ frameRate);  

    //loop through all bullets for this enemy to move and update position, if bullet off edge of screen despawn bullet
    for (int i = 0; i < bullets.size(); i++) {
      EnemyBullet b = bullets.get(i);
      b.display();
      if (b.position.x < 0) {
        bullets.remove(i);
      }
    }
    
    //randomize bullet spawn
     if (random(0, 40) < 1 && shootingCooldown <= 0 && bullets.size() < bulletMax)
    {
      bullets.add(new EnemyBullet(new PVector(position.x,position.y)));
      shootingCooldown = shootingCooldownMax;
    }

 }
   

  
  
  void initialize() {

    //assign random speed for variety
    speed = random(3, 6);

    //sets enemy spawn location at right side of screen along random Y value
    position = new PVector(width, random(0, height));

  }
    
}
