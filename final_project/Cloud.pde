//class to represent a cloud in the background
class Cloud {
 //define variables to contain cloud image, speed of movement, size, and position
 PImage cloudImage; 
 float speed;
 float size;
 PVector position;

 Cloud() {
  
   //load and initialize cloud image
   cloudImage = loadImage("LargeCloud1.png");  
   initialize();
   
 }
 
 //displays cloud
 void display() {
    //if cloud beyond left edge of screen, reset cloud
    if (position.x <= 0) {
    initialize();
    }
   
   //moves cloud position and redraw
   position.x -=speed;  
   imageMode(CENTER);
   image(cloudImage, position.x, position.y, size * 2 ,size);
   
 }
  void initialize() {

    //assign random cloud size, and calculates speed as an inverse to size 
    size = random(5, 30);
    speed = 40/size;

    //sets cloud spawn location beyond right edge of screen along random Y value
    position = new PVector(width + random(0, width), random(0, height));

  }
}
