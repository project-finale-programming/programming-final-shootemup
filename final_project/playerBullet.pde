/*
Bullet

Object definition for bullets, fired out from player ship
*/


class PlayerBullet {
  //variables for bullet position, size, speed, and image
  PVector position;
  PVector size = new PVector(8, 5);
  float speed;  
  PImage playerBulletImage;
  
 //Constructor takes in position the bullet will be spawned at. Default speed is set here
  PlayerBullet(PVector bulletPosition)
  {
    position = bulletPosition;
    speed = 10;
    
    playerBulletImage = loadImage("BulletPlayer.png");
  }
 
 //Move and redisplay bullet
 void display() {
    position.x += speed;
    
    imageMode(CENTER);
    image(playerBulletImage, position.x, position.y, size.x, size.y);
  }

}
