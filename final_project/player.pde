//creating player class
class Player {
 //declaring vectors to define position and size of plane
 PVector position;
 PVector planeSize = new PVector (35, 35);
 
 //variable to define speed plane moves across screen
 float planeVelocity = 5;
 
 //holds player image
 PImage playerPlaneImage;
 
 //there will be a cooldown for shooting, and number of bullets on the window will also be limited
 float bulletMax = 15, shootingCooldown, shootingCooldownMax = 0.1;
 
 //we will store all our playerBullets in this ArrayList
 ArrayList<PlayerBullet> bullets;
 
 Player(){
   //initializing variable to define plane position on screen, and loading image
   position = new PVector(planeInitialPosition.x, planeInitialPosition.y);   
   playerPlaneImage = loadImage("Player.png");
   
    //initialize bullets ArrayList
    bullets = new ArrayList<PlayerBullet>();
    
    shootingCooldown = shootingCooldownMax;
  
 }
   void display(){

     //display plane image
     imageMode(CENTER);
     image(playerPlaneImage, position.x, position.y, planeSize.x, planeSize.y);
     
     //when key is pressed move plane in direction of depressed arrow key and constrains it within allowable boundries
     position.x = constrain(position.x + planeVelocity * planeSteer.x, planeSize.x/2, width - planeSize.x/2);
     position.y = constrain(position.y + planeVelocity * planeSteer.y, planeSize.y/2, height - planeSize.y/2);

    //reduce shootingCooldown. we are using 1/frameRate to use second as the unit for the cooldown values 
    shootingCooldown -= (1/ frameRate);  

    //loop through all bullets for the player to move and update position, if bullet off edge of screen despawn bullet
    for (int i = 0; i < bullets.size(); i++) {
      PlayerBullet b = bullets.get(i);
      b.display();
      if (b.position.x > width) {
        bullets.remove(i);
      }
    }


   }
   
  //if cooldown period has passed, and there are less than max amount of bullets on the screen, spawn a new bullet and add it to the arraylist, then reset cooldown.
  void shoot()
  {

    if (shootingCooldown <= 0 && bullets.size() < bulletMax)
    {
      bullets.add(new PlayerBullet(new PVector(position.x,position.y)));
      shootingCooldown = shootingCooldownMax;
    }
  }
}
