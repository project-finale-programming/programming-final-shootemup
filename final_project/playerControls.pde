//event handler for checking if key is pressed

//Declaring vector to determine which key is pressed to steer plane, shoot the guns, and restart game at game over screen.
//this reduces delay when key is held down
PVector planeSteer = new PVector (0,0);

//Delcaring variable for shoot key
int planeShoot = 0;

int gameReset = 0;

void keyPressed()
{
   if (keyCode==UP){
     planeSteer.y = -1;
   }
   
 if (keyCode==DOWN){
     planeSteer.y = 1;
   }  
   
   if (keyCode==LEFT){
     planeSteer.x = -1;
   }  
   
   if (keyCode==RIGHT){
     planeSteer.x = 1;
   }
   
   if (keyCode==SHIFT){
     planeShoot = 1;
   }
   
   if (keyCode==ENTER){
     gameReset = 1;
   }

  }

//event handler to check if key is released
void keyReleased()
{
  
  if (keyCode ==UP || keyCode ==DOWN){
   planeSteer.y = 0;
  }
  
  if (keyCode ==LEFT || keyCode ==RIGHT){
   planeSteer.x = 0;
  }
 
  if (keyCode==SHIFT){
     planeShoot = 0;
   }
   
  if (keyCode==ENTER){
     gameReset = 0;
   }
}
