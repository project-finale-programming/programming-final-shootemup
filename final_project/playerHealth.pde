class PlayerHealth {
  //declaring variables for instrument position
  PVector position = new PVector(50, 50);
  
//declaring variables for player health
int playerHealthValue = playerHealthInitialValue;

  
  //recieving position to display instrument on-screen from object instantiation
  PlayerHealth(){

  }
  
  
  //method to update bar graph value of instrument
  void display(int playerHealthDecreaseAmount){

    //draw static parts of instrument
    fill(203,231,255);
    rect(position.x-10, position.y+40, 60,-100);
    
    //display text labeling instrument
    fill(0);
    text("Health", position.x, position.y+37);
    
    playerHealthValue = constrain(playerHealthValue - playerHealthDecreaseAmount, 0, playerHealthInitialValue);
    //display bar graph sizing it relative to health value
    fill(255,0,0);
    rect(position.x, position.y - 10, 40, -35 * playerHealthValue / playerHealthInitialValue);
    text(playerHealthValue, position.x + 8, position.y + 10);

  }
}
