/*
enemy Bullet

Object definition for bullets, fired out from enemy ship
*/


class EnemyBullet {
  //Bullet position, size and speed variables
  PVector position;
  PVector size = new PVector(8, 5);
  float speed; 
  PImage enemyBulletImage;
  
 //Constructor takes in position the bullet will be spawned at. Default speed is set here
  EnemyBullet(PVector bulletPosition)
  {
    position = bulletPosition;
    speed = 6;
    
    enemyBulletImage = loadImage("EnemyBullet.png");
  }
 
 //Move and redisplay bullet
 void display() {
    position.x -= speed;
    
    imageMode(CENTER);
    image(enemyBulletImage, position.x, position.y, size.x, size.y);
  }

}
