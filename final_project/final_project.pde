/* 
The player controls a plane with the arrow keys and shoots with the shift key to destroy enemies. Enemies will fire randomly at the player, if the player hits an enemy or is hit by a bullet the player will take damage. 
At zero health game over. At the game over screen the player may press enter to restart the game.
*/

//declaring vector for plane initial position
PVector planeInitialPosition = new PVector (50,200);

//declaring variable for player health
int playerHealthInitialValue = 100;

//declaring playerPlane object
Player playerPlane;

//variable for distance between player and enemy to create a collision
float playerEnemyCollision = 30;

//variable for amount of damage taken by player on collision
int damageTaken = 10;

//array for enemies
ArrayList<Enemy> enemies;
int numEnemies = 6;

//declaring array for clouds in background of screen
Cloud [] clouds = new Cloud[40];

//declaring playerHealth object
PlayerHealth playerHealth;

void setup(){
  //setting screen size
  size(800,500);
  
  //calling player class
  playerPlane = new Player();
  
  //calling playerHealth class
  playerHealth = new PlayerHealth();
  
      //initialize enemies and add them to the arraylist
    enemies = new ArrayList<Enemy>();
    for (int i = 0; i < numEnemies; i++) {
      enemies.add(new Enemy());
    }
    
    //initialize array of clouds
    for (int i = 0; i < clouds.length; i++) {
    clouds[i] = new Cloud();
   }
}

void draw(){
  //setting background
  background(165,180,255);
  
  //display clouds
   for (int i = 0; i < clouds.length; i++) {
     clouds[i].display();
   }

   //detect if player has pressed fire button, if so shoot bullet
   if (planeShoot == 1) {
     playerPlane.shoot();
   }

  //display playerPlane
  playerPlane.display();
  
  //Collision detection between enemies, players, and bullets
   for (int i = 0; i < enemies.size(); i++) {
     //move the enemy
      enemies.get(i).display();
      //check for collision between player and enemy, if collision reset enemy and decrease player health
      if (collisionDetection (playerPlane.position, enemies.get(i).position, playerEnemyCollision)) {
        enemies.get(i).initialize();
        playerHealth.display(damageTaken);
      }

       //process each player bullet to determine if it hits an enemy
       for (int j = 0; j < playerPlane.bullets.size(); j++) {
          //check for collision between player bullet and enemy, if collision reset enemy and remove player bullet
          if (collisionDetection (playerPlane.bullets.get(j).position, enemies.get(i).position, playerEnemyCollision)) {
            enemies.get(i).initialize();
            playerPlane.bullets.remove(j);
          }
        }              

       //process each enemy bullet to determine if it hits the player plane
       for (int k = 0; k < enemies.get(i).bullets.size(); k++) {
          //check for collision between enemy bullet and player planer, if collision decrease player health and remove enemy bullet
          if (collisionDetection (playerPlane.position, enemies.get(i).bullets.get(k).position, playerEnemyCollision)) {
            enemies.get(i).bullets.remove(k);
            playerHealth.display(damageTaken);
          }
        }              
  

}
 

      
      
  //display playerHealth
  playerHealth.display(0);
  
  //if player health is 0 or less, trigger game over.
  if(playerHealth.playerHealthValue <= 0) {
    fill(0);
    rect(0, 0, width, height);
    fill(255);
    textAlign(CENTER);
    text("Game Over", width/2, 200);
    text("Press Enter to Continue", width/2, 240);
    
    //detects game reset, if so, reset enemies, player health, and player position
    if (gameReset == 1) {
     playerHealth.playerHealthValue = playerHealthInitialValue; 
    for (int i = 0; i < enemies.size(); i++) {
     enemies.get(i).initialize();
     playerPlane.position = new PVector(planeInitialPosition.x, planeInitialPosition.y);
     }
    }
  }
    
}

//function to detect collision between two objects
boolean collisionDetection(PVector position1, PVector position2, float accuracy) {
  return(PVector.dist(position1, position2) < accuracy);
}
